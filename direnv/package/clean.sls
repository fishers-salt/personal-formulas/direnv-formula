# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as direnv with context %}

include:
  - {{ sls_config_clean }}

direnv-package-install-pkg-removed:
  pkg.removed:
    - name: {{ direnv.pkg.name }}
