# frozen_string_literal: true

control 'direnv-package-install-pkg-installed' do
  title 'should be installed'

  describe package('direnv') do
    it { should be_installed }
  end
end
