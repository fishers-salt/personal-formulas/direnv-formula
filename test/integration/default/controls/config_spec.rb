# frozen_string_literal: true

control 'direnv-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'direnv-config-file-zsh-rc-include-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/rc_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'direnv-config-file-zsh-rc-includes-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/rc_includes/direnv.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('if command -v direnv > /dev/null; then') }
  end
end
