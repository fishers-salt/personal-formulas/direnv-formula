# frozen_string_literal: true

control 'direnv-package-clean-pkg-absent' do
  title 'should not be installed'

  describe package('direnv') do
    it { should_not be_installed }
  end
end
