# frozen_string_literal: true

control 'direnv-config-clean-zsh-rc-includes-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/zsh/rc_includes/direnv.zsh') do
    it { should_not exist }
  end
end
